import { Component, OnInit } from '@angular/core';
import { AutoCompleteService } from '../auto-complete.service';

@Component({
  selector: 'app-search-address',
  templateUrl: './search-address.component.html',
  styleUrls: ['./search-address.component.css']
})
export class SearchAddressComponent implements OnInit {
  addresses: string[];
  input: string;

  constructor(private autoComplete: AutoCompleteService) { }

  ngOnInit() {
  }

  search() {
    if (this.input) {
      this.autoComplete.getAddresses(this.input)
        .subscribe(response => this.addresses = response,
          error => console.log(error));
    } else {
      this.addresses = [];
    }
  }


}
