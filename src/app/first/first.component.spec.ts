import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstComponent } from './first.component';

describe('FirstComponent', () => {
  let component: FirstComponent;
  let fixture: ComponentFixture<FirstComponent>;
  let element:HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //test case qui vérifie que lorsqu'on déclenche
  //la méthode changeMessage du component, la valeur de la propriété
  //message est bien modifiée (pas besoin de querySelector ni de fixture)
  it('should change message on changeMessage call', () => {
    expect(component.message).toEqual('Base message');
    component.changeMessage();
    expect(component.message).not.toEqual('Base message');
    expect(component.message).toEqual('Something else');
  });

  //Créer un autre test case qui vérifie si notre component affiche
  //bien le message dans un paragraphe (là yaura besoin de la 
  //fixture.nativeElement et de querySelector)
  it('should display message in template', ()=> {
    let p = element.querySelector('p');
    expect(p.textContent).toEqual('Base message');
  });
  //Pourquoi pas faire un autre test qui vérifie si l'affichage est bien
  //modifié quand on déclenche la méthode changeMessage
  it('should display modified message in template', () => {
    let p = element.querySelector('p');
    let btn = element.querySelector('button');
    btn.click();
    // component.changeMessage();
    fixture.detectChanges();
    expect(p.textContent).toEqual('Something else');

  });

  

});
